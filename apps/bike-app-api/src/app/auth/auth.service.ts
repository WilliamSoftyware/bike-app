import { Injectable, UnauthorizedException } from '@nestjs/common';
import { of, throwError } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UserService } from '../user/service/user.service';

@Injectable()
export class AuthService {
  constructor(private userService: UserService) {}

  credentialsAreValid(email: string, password: string) {
    return this.userService
      .getByEmail(email)
      .pipe(
        map(async (user) => {
          switchMap((isValid) =>
            isValid ? of(isValid) : throwError(new UnauthorizedException())
          );
          return !!user && password === user.password ? user : null;
        })
      )
      .toPromise();
  }
}
