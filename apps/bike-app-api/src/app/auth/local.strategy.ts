import * as bcrypt from 'bcrypt';
import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { of, throwError } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UserService } from '../user/service/user.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private userService: UserService) {
    super({ usernameField: 'email', passwordField: 'password' });
  }

  validate(email: string, password: string) {
    if (!email || !password) {
      throw new HttpException(
        'Veuillez saisir un email et/ou un mot de passe valide.',
        HttpStatus.BAD_REQUEST
      );
    }
    return this.userService
      .getByEmail(email)
      .pipe(
        map(async (user) => {
          switchMap((isValid) =>
            isValid ? of(isValid) : throwError(new UnauthorizedException())
          );
          if (!user || !(await bcrypt.compare(password, user.password))) {
            throw new HttpException(
              "Le mot de passe ou l'adresse email est incorrect",
              HttpStatus.UNAUTHORIZED
            );
          }
          return user;
        })
      )
      .toPromise();
  }
}
