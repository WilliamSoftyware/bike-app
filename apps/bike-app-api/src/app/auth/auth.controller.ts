import { Controller, Post, Request, UseGuards } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthGuard } from '@nestjs/passport';
import { authResourcePath, UserDto } from '@bike-app/bike-api-resource-lib';
import { Request as ExpressRequest } from 'express';
@Controller(authResourcePath)
export class AuthController {
  constructor(private jwtService: JwtService) {}

  @UseGuards(AuthGuard('local'))
  @Post('/login')
  async login(@Request() request: ExpressRequest) {
    const { id, role } = request.user as UserDto;
    const payload = { id: id, role: role };
    return this.jwtService.signAsync(payload);
  }
}
