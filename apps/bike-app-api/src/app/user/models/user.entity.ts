import { Entity, Column } from 'typeorm';
import { UserRole } from '@bike-app/bike-api-resource-lib';
import { BaseEntity } from '../../base-entity';

@Entity()
export class UserEntity extends BaseEntity {
  @Column('text', { nullable: false })
  firstName: string;

  @Column('text', { nullable: false })
  lastName: string;

  @Column({ name: "email", unique: true, nullable: false })
  email: string;

  @Column()
  password: string;

  @Column({ type: 'enum', enum: UserRole, default: UserRole.USER })
  role: UserRole;
}
