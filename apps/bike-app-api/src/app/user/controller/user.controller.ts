import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { UserDto, userResourcePath } from '@bike-app/bike-api-resource-lib';
import { UserService } from '../service/user.service';
import { map } from 'rxjs/operators';
import { AuthGuard } from '@nestjs/passport';
import { RoleGuardGuard } from '../../auth/role-guard.guard';
import { SetMetadata } from '@nestjs/common';

@Controller(userResourcePath)
export class UserController {
  constructor(private userService: UserService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin'])
  @HttpCode(HttpStatus.OK)
  getUsers(): Promise<UserDto[]> {
    return this.userService.getUsers();
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin', 'user'])
  getUserById(@Param('id', ParseIntPipe) id: number): Promise<UserDto> {
    return this.userService.getUserById(id);
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin'])
  @HttpCode(HttpStatus.OK)
  deleteUser(
    @Param('id', ParseIntPipe) id: number
  ): Promise<HttpException | { message: string }> {
    return this.userService.deleteUser(id);
  }

  @Post('')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin'])
  createUser(
    @Body() userToCreate: UserDto
  ): Promise<HttpException | { message: string }> {
    return this.userService.createUser(userToCreate);
  }

  @Patch('/:id')
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin'])
  @HttpCode(HttpStatus.OK)
  updateUser(
    @Param('id', ParseIntPipe) id: number,
    @Body() userToUpdate: UserDto
  ): Promise<HttpException | { message: string }> {
    return this.userService.updateUser(id, {
      ...userToUpdate,
    });
  }

  // Check credentials:
  credentialsAreValid(email: string, password: string) {
    return this.userService
      .getByEmail(email)
      .pipe(map((user) => user && user.password == password));
  }
}
