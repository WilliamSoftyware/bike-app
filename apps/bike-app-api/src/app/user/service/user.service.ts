import * as bcrypt from 'bcrypt';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { catchError, map } from 'rxjs/operators';
import { from, Observable } from 'rxjs';
import { Repository, UpdateResult } from 'typeorm';
import { UserEntity } from '../models/user.entity';
import { UserDto } from '@bike-app/bike-api-resource-lib';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>
  ) {}

  // Retourne une Promise<UserEntity[]>, la liste des utilisateurs (USER/MANAGER/ADMIN)
  getUsers(): Promise<UserEntity[]> {
    return from(this.userRepository.find()).toPromise();
  }

  // Retourne un Promise<UserEntity> en utilisant la méthode findOne du userRepository (nécessite: un id qui existe dans la base de données)
  async getUserById(id: number): Promise<UserEntity> {
    if (!id) {
      throw new HttpException(
        'Veuillez saisir un idenfiant valide.',
        HttpStatus.BAD_REQUEST
      );
    }

    return await from(this.userRepository.findOne({ id }))
      .pipe(
        catchError(() => {
          throw new HttpException(
            "Un problème a été rencontré pendant la lecture des informations de l'utilisateur.",
            HttpStatus.BAD_REQUEST
          );
        }),
        map((user: UserEntity) => {
          if (!user) {
            throw new HttpException(
              "Cet utilisateur n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          const { ...result } = user;
          return result;
        })
      )
      .toPromise();
  }

  // Retourne un Observable<UserEntity> en utilisant la méthode findOne du userRepository (nécessite: une email qui existe dans la base de données)
  getByEmail(emailDto: string): Observable<UserEntity> {
    return from(
      this.userRepository.findOne({ where: { email: emailDto.toLowerCase() } })
    ).pipe(
      catchError(() => {
        throw new HttpException(
          "Un problème a été rencontré pendant la lecture des informations de l'utilisateur.",
          HttpStatus.BAD_REQUEST
        );
      }),
      map((user: UserEntity) => {
        if (!user) {
          throw new HttpException(
            "Cet email n'est pas reconnu dans la base de données.",
            HttpStatus.BAD_REQUEST
          );
        }
        return user;
      })
    );
  }

  // Ajoute un utilisateur dans la base de donnée en utilisant la méthode save du userRepository (nécessite: mot de passe, firstName, lastName et l'email)
  async createUser(
    userDto: UserDto
  ): Promise<HttpException | { message: string }> {
    // Vérification des données dans l'objet UserDto (il manques des informations)
    if (
      !userDto.firstName ||
      !userDto.lastName ||
      !userDto.email ||
      !userDto.password
    ) {
      throw new HttpException(
        'Veuillez saisir toutes les informations pour créer le compte.',
        HttpStatus.BAD_REQUEST
      );
    }

    // Extraction des données de l'objet UserDto
    const { firstName, lastName, email, password, role } = userDto;

    // Vérifie d'abord que l'utilisateur n'existe pas dans la base de données
    const userInDb = await this.userRepository.findOne({
      where: { email: email.toLowerCase() },
    });
    if (userInDb) {
      throw new HttpException(
        "L'utilisateur existe déjà.",
        HttpStatus.BAD_REQUEST
      );
    }

    // Création de l'objet newUser en utilisant les données de UserDto
    const newUser = new UserEntity();
    newUser.firstName = firstName;
    newUser.lastName = lastName;
    newUser.email = email.toLowerCase();
    newUser.password = await bcrypt.hash(password, 10);
    newUser.role = role;

    // Enregistre les informations dans la base de données en utilisant la méthode save du userRepository
    return from(this.userRepository.save(newUser))
      .pipe(
        catchError(() => {
          throw new HttpException(
            `Ce compte existe déjà.`,
            HttpStatus.BAD_REQUEST
          );
        }),
        map((user: UserEntity) => {
          return {
            message: `Le compte avec l'id: ${user.id}, a bien était créé !`,
          };
        })
      )
      .toPromise();
  }

  // Supprime un utilisateur en utilisant la méthode delete du userRepository (nécessite: un id qui existe dans la base de données)
  async deleteUser(id: number): Promise<HttpException | { message: string }> {
    return from(this.userRepository.delete(id))
      .pipe(
        map((user: UpdateResult) => {
          if (user.affected === 0) {
            throw new HttpException(
              "Cet utilisateur n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          return {
            message: `Le compte avec l'id: ${id}, a bien était supprimé !`,
          };
        })
      )
      .toPromise();
  }

  // Met à jours un utilisateur en utilisant la méthode save du userRepository (nécessite: un id qui existe dans la base de donnée et les informations à modifier)
  async updateUser(
    id: number,
    userDto: UserDto
  ): Promise<HttpException | { message: string }> {
    const userEntity = this.getUserById(id);
    const userToUpdate = new UserEntity();

    const accountAlreadyExist = await this.userRepository.find({
      where: { email: userDto.email.toLowerCase() },
    });

    if (accountAlreadyExist.length > 0) {
      if (
        accountAlreadyExist[0].email.toLowerCase() ==
        (await userEntity).email.toLowerCase()
      ) {
        throw new HttpException(
          'Vous utilisez déjà cette adresse email.',
          HttpStatus.BAD_REQUEST
        );
      }
      throw new HttpException(
        "Cette adresse email est déjà utilisée par quelqu'un d'autre.",
        HttpStatus.BAD_REQUEST
      );
    }

    // Récupère les informations de l'utilisateur qui a cet identifiant
    userToUpdate.id = (await userEntity).id;
    userToUpdate.firstName = (await userEntity).firstName;
    userToUpdate.lastName = (await userEntity).lastName;
    userToUpdate.email = (await userEntity).email.toLowerCase();
    userToUpdate.password = (await userEntity).password;
    userToUpdate.role = (await userEntity).role;

    // Extraction des données de l'objet UserDto
    const { firstName, lastName, email, password, role } = userDto;

    if (firstName) {
      userToUpdate.firstName = firstName;
    }
    if (lastName) {
      userToUpdate.lastName = lastName;
    }
    if (email) {
      userToUpdate.email = email.toLowerCase();
    }
    if (password) {
      userToUpdate.password = await bcrypt.hash(password, 10);
    }
    if (role) {
      userToUpdate.role = role;
    }
    userToUpdate.updatedAt = new Date(Date.now());

    return from(this.userRepository.save(userToUpdate))
      .pipe(
        map((user: UserEntity) => {
          if (!user) {
            throw new HttpException(
              "Cet utilisateur n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          return {
            message: `Le compte avec l'id: ${user.id}, a bien était modifié !`,
          };
        })
      )
      .toPromise();
  }
}
