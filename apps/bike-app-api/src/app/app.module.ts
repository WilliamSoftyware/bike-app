import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { ParcModule } from './parc/parc.module';
import { StationModule } from './station/station.module';
import { BikeModule } from './bike/bike.module';
import { AuthModule } from './auth/auth.module';
import { RealTimePositionModule } from './event/real-time-position.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'bike-app-client')
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'remotemysql.com',
      port: 3306,
      username: 'ZDiXOsH1Xf',
      password: 'LciTXx0cYb',
      database: 'ZDiXOsH1Xf',
      autoLoadEntities: true,
      synchronize: false,
    }),
    UserModule,
    ParcModule,
    StationModule,
    BikeModule,
    AuthModule,
    RealTimePositionModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
