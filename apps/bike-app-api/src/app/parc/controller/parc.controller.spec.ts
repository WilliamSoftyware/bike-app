import { Test, TestingModule } from '@nestjs/testing';
import { ParcController } from './parc.controller';

describe('ParcController', () => {
  let controller: ParcController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ParcController],
    }).compile();

    controller = module.get<ParcController>(ParcController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
