import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  SetMetadata,
  UseGuards,
} from '@nestjs/common';
import { ParcDto, parcResourcePath } from '@bike-app/bike-api-resource-lib';
import { ParcService } from '../service/parc.service';
import { AuthGuard } from '@nestjs/passport';
import { RoleGuardGuard } from '../../auth/role-guard.guard';

@Controller(parcResourcePath)
export class ParcController {
  constructor(private parcService: ParcService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  getParcs(): Promise<ParcDto[]> {
    return this.parcService.getParcs();
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'))
  getParcById(@Param('id', ParseIntPipe) id: number): Promise<ParcDto> {
    return this.parcService.getParcById(id);
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin'])
  @HttpCode(HttpStatus.OK)
  deleteParc(
    @Param('id', ParseIntPipe) id: number
  ): Promise<HttpException | { message: string }> {
    return this.parcService.deleteParc(id);
  }

  @Post('')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'))
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin'])
  createParc(
    @Body() userToCreate: ParcDto
  ): Promise<HttpException | { message: string }> {
    return this.parcService.createParc(userToCreate);
  }

  @Patch('/:id')
  @UseGuards(AuthGuard('jwt'))
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin'])
  updateParc(
    @Param('id', ParseIntPipe) id: number,
    @Body() userToUpdate: ParcDto
  ): Promise<HttpException | { message: string }> {
    return this.parcService.updateParc(id, {
      ...userToUpdate,
    });
  }
}
