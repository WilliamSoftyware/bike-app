import { Module } from '@nestjs/common';
import { ParcService } from './service/parc.service';
import { ParcController } from './controller/parc.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParcEntity } from './models/parc.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ParcEntity])],
  providers: [ParcService],
  controllers: [ParcController],
  exports: [ParcService],
})
export class ParcModule {}
