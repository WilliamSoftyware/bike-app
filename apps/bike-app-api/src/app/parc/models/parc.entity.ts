import { Entity, Column } from 'typeorm';
import { BaseEntity } from '../../base-entity';

@Entity()
export class ParcEntity extends BaseEntity {
  @Column('text', { nullable: false })
  name!: string;

  @Column({ type: 'decimal', precision: 8, scale: 6, nullable: false })
  longitude: number;

  @Column({ type: 'decimal', precision: 8, scale: 6, nullable: false })
  latitude: number;

  @Column('text', { nullable: false })
  city!: string;
}
