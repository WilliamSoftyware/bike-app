import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { catchError, map } from 'rxjs/operators';
import { from } from 'rxjs';
import { Repository, UpdateResult } from 'typeorm';
import { ParcEntity } from '../models/parc.entity';
import { ParcDto } from '@bike-app/bike-api-resource-lib';

@Injectable()
export class ParcService {
  constructor(
    @InjectRepository(ParcEntity)
    private readonly parcRepository: Repository<ParcEntity>
  ) {}

  // Retourne une Promise<ParcEntity[]>, la liste des parcs
  getParcs(): Promise<ParcEntity[]> {
    return from(this.parcRepository.find()).toPromise();
  }

  // Retourne un Promise<ParcEntity> en utilisant la méthode findOne du parcRepository (nécessite: un id qui existe dans la base de données)
  getParcById(id: number): Promise<ParcEntity> {
    if (!id) {
      throw new HttpException(
        'Veuillez saisir un idenfiant valide.',
        HttpStatus.BAD_REQUEST
      );
    }

    return from(this.parcRepository.findOne({ id }))
      .pipe(
        catchError(() => {
          throw new HttpException(
            'Un problème a été rencontré pendant la lecture des informations du parc.',
            HttpStatus.BAD_REQUEST
          );
        }),
        map((parc: ParcEntity) => {
          if (!parc) {
            throw new HttpException(
              "Ce parc n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          const { ...result } = parc;
          return result;
        })
      )
      .toPromise();
  }

  // Créer un parc dans la base de donnée en utilisant la méthode save du parcRepository (nécessite: le nom, la latitude, la longitude et la ville)
  async createParc(
    parcDto: ParcDto
  ): Promise<HttpException | { message: string }> {
    // Vérification des données dans l'objet ParcDto (il manques des informations)
    if (
      !parcDto.name ||
      !parcDto.latitude ||
      !parcDto.longitude ||
      !parcDto.city
    ) {
      throw new HttpException(
        'Veuillez saisir toutes les informations pour créer le parc.',
        HttpStatus.BAD_REQUEST
      );
    }

    // Extraction des données de l'objet ParcDto
    const { name, latitude, longitude, city } = parcDto;

    // Vérifie d'abord que le parc n'existe pas dans la base de données
    const parcInDb = await this.parcRepository.findOne({
      where: { name },
    });
    if (parcInDb) {
      throw new HttpException('Le parc existe déjà.', HttpStatus.BAD_REQUEST);
    }

    // Création de l'objet newParc en utilisant les données de ParcDto
    const newParc = new ParcEntity();
    newParc.name = name;
    newParc.latitude = latitude;
    newParc.longitude = longitude;
    newParc.city = city;

    // Enregistre les informations dans la base de données en utilisant la méthode save du parcRepository
    return from(this.parcRepository.save(newParc))
      .pipe(
        catchError(() => {
          throw new HttpException(
            'Un problème a été rencontré pendant la création du parc.',
            HttpStatus.BAD_REQUEST
          );
        }),
        map((parc: ParcEntity) => {
          return {
            message: `Le parc avec l'id: ${parc.id}, a bien était créé !`,
          };
        })
      )
      .toPromise();
  }

  // Supprime un parc en utilisant la méthode delete du parcRepository (nécessite: un id qui existe dans la base de données)
  deleteParc(id: number): Promise<HttpException | { message: string }> {
    return from(this.parcRepository.delete(id))
      .pipe(
        map((parc: UpdateResult) => {
          if (parc.affected === 0) {
            throw new HttpException(
              "Ce parc n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          return {
            message: `Le parc avec l'id: ${id}, a bien était supprimé !`,
          };
        })
      )
      .toPromise();
  }

  // Met à jours un parc en utilisant la méthode save du parcRepository (nécessite: un id qui existe dans la base de donnée et les informations à modifier)
  async updateParc(
    id: number,
    parcDto: ParcDto
  ): Promise<HttpException | { message: string }> {
    const parcEntity = this.getParcById(id);
    const parcToUpdate = new ParcEntity();

    // Extraction des données de l'objet ParcDto
    const { name, latitude, longitude, city } = parcDto;

    // Récupère les informations du parc qui a cet identifiant
    parcToUpdate.id = (await parcEntity).id;
    parcToUpdate.name = (await parcEntity).name;
    parcToUpdate.latitude = (await parcEntity).latitude;
    parcToUpdate.longitude = (await parcEntity).longitude;
    parcToUpdate.city = (await parcEntity).city;

    if (name) {
      parcToUpdate.name = name;
    }
    if (latitude) {
      parcToUpdate.latitude = latitude;
    }
    if (longitude) {
      parcToUpdate.longitude = longitude;
    }
    if (city) {
      parcToUpdate.city = city;
    }
    parcToUpdate.updatedAt = new Date(Date.now());

    return from(this.parcRepository.save(parcToUpdate))
      .pipe(
        map((parc: ParcEntity) => {
          if (!parc) {
            throw new HttpException(
              "Ce parc n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          return {
            message: `Le parc avec l'id: ${parc.id}, a bien était modifié !`,
          };
        })
      )
      .toPromise();
  }
}
