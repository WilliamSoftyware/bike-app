import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BikeController } from './controller/bike.controller';
import { BikeEntity } from './models/bike.entity';
import { BikeService } from './service/bike.service';

@Module({
  imports: [TypeOrmModule.forFeature([BikeEntity])],
  controllers: [BikeController],
  providers: [BikeService],
  exports: [BikeService],
})
export class BikeModule {}

