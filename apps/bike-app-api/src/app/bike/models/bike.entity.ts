import { Entity, Column, JoinColumn, ManyToOne, Index } from 'typeorm';
import { UserEntity } from '../../user/models/user.entity';
import { StationEntity } from '../../station/models/station.entity';
import { BaseEntity } from '../../base-entity';

@Entity()
export class BikeEntity extends BaseEntity {
  @Column({
    type: 'decimal',
    precision: 8,
    scale: 6,
    nullable: false,
  })
  longitude: number;

  @Column({
    type: 'decimal',
    precision: 8,
    scale: 6,
    nullable: false,
  })
  latitude: number;

  @Index({ unique: true })
  @ManyToOne(() => UserEntity || null, { onDelete: 'SET NULL' })
  @JoinColumn()
  user?: UserEntity;

  @Index({ unique: false })
  @ManyToOne(() => StationEntity || null, { onDelete: 'SET NULL' })
  @JoinColumn()
  station?: StationEntity;
}
