import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
  ParseIntPipe,
  HttpException,
  UseGuards,
  SetMetadata,
} from '@nestjs/common';
import { BikeService } from '../service/bike.service';
import { BikeDto, bikeResourcePath } from '@bike-app/bike-api-resource-lib';
import { AuthGuard } from '@nestjs/passport';
import { RoleGuardGuard } from '../../auth/role-guard.guard';

@Controller(bikeResourcePath)
export class BikeController {
  constructor(private readonly bikeService: BikeService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  getBikes(): Promise<BikeDto[]> {
    return this.bikeService.getBikes();
  }

  @Get('/byStation:id')
  @UseGuards(AuthGuard('jwt'))
  getBikesByStationId(
    @Param('id', ParseIntPipe) id: number
  ): Promise<BikeDto[]> {
    return this.bikeService.getBikesByStationId(id);
  }

  @Get('/byUser/:id')
  @UseGuards(AuthGuard('jwt'))
  getBikeByUserId(@Param('id', ParseIntPipe) id: number): Promise<BikeDto> {
    return this.bikeService.getBikeByUserId(id);
  }

  @Get('/:id')
  getBikeById(@Param('id', ParseIntPipe) id: number): Promise<BikeDto> {
    return this.bikeService.getBikeById(id);
  }

  @Delete('/:id')
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin', 'manager'])
  @HttpCode(HttpStatus.OK)
  deleteBike(
    @Param('id', ParseIntPipe) id: number
  ): Promise<HttpException | { message: string }> {
    return this.bikeService.deleteBike(id);
  }

  @Post('')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin', 'manager'])
  createBike(
    @Body() bikeToCreate: BikeDto
  ): Promise<HttpException | { message: string }> {
    return this.bikeService.createBike(bikeToCreate);
  }

  @Patch('/:id')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  updateBike(
    @Param('id', ParseIntPipe) id: number,
    @Body() bikeToUpdate: BikeDto
  ): Promise<HttpException | { message: string }> {
    return this.bikeService.updateBike(id, {
      ...bikeToUpdate,
    });
  }
}
