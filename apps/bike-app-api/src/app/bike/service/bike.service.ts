import { BikeDto } from '@bike-app/bike-api-resource-lib';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { from } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Repository, UpdateResult } from 'typeorm';
import { BikeEntity } from '../models/bike.entity';

@Injectable()
export class BikeService {
  constructor(
    @InjectRepository(BikeEntity)
    private readonly bikeRepository: Repository<BikeEntity>
  ) {}

  // Retourne une Promise<BikeEntity[]>, la liste des vélos
  getBikes(): Promise<BikeEntity[]> {
    return from(this.bikeRepository.find()).toPromise();
  }

  // Retourne une Promise<BikeEntity[]>, la liste des vélos présents dans une station
  getBikesByStationId(id: number): Promise<BikeEntity[]> {
    return from(
      this.bikeRepository
        .createQueryBuilder('getBikesByStationId')
        .where('bike.stationId = :id', { id })
        .getMany()
    ).toPromise();
  }

  // Retourne une Promise<BikeEntity>, le vélo utilisé par l'utilisateur
  getBikeByUserId(id: number): Promise<BikeEntity> {
    return from(
      this.bikeRepository
        .createQueryBuilder('getBikeByUserId')
        .where('userId = :id', { id })
        .getOne()
    ).toPromise();
  }

  // Retourne un Promise<BikeEntity> en utilisant la méthode findOne du userRepository (nécessite: un id qui existe dans la base de données)
  getBikeById(id: number): Promise<BikeEntity> {
    if (!id) {
      throw new HttpException(
        'Veuillez saisir un identifiant valide.',
        HttpStatus.BAD_REQUEST
      );
    }

    return from(this.bikeRepository.findOne({ id }))
      .pipe(
        catchError(() => {
          throw new HttpException(
            "Un problème a été rencontré pendant la lecture des informations de l'utilisateur.",
            HttpStatus.BAD_REQUEST
          );
        }),
        map((user: BikeEntity) => {
          if (!user) {
            throw new HttpException(
              "Ce vélo n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          const { ...result } = user;
          return result;
        })
      )
      .toPromise();
  }

  // Créer un vélo dans la base de donnée en utilisant la méthode save du bikeRepository (nécessite: la longitude, la latitude, la station et l'utilisateur)
  async createBike(
    bikeDto: BikeDto
  ): Promise<HttpException | { message: string }> {
    // Vérification des données dans l'objet BikeDto (il manques des informations)
    if (!bikeDto.longitude || !bikeDto.latitude) {
      throw new HttpException(
        'Veuillez saisir toutes les informations pour créer le vélo.',
        HttpStatus.BAD_REQUEST
      );
    }

    // Extraction des données de l'objet BikeDto
    const { longitude, latitude, station, user } = bikeDto;

    // Vérifie d'abord que le vélo n'est pas déjà dans la base de données
    const veloInDb = await this.bikeRepository.findOne({
      where: { user },
    });
    if (veloInDb) {
      throw new HttpException(
        'Un utilisateur utilise déjà ce vélo.',
        HttpStatus.NOT_FOUND
      );
    }

    const newBike = new BikeEntity();
    newBike.longitude = longitude;
    newBike.latitude = latitude;
    newBike.station = station;
    newBike.user = user;

    return from(this.bikeRepository.save(newBike))
      .pipe(
        catchError(() => {
          throw new HttpException(
            'Un problème a été rencontré pendant la création du vélo ',
            HttpStatus.BAD_REQUEST
          );
        }),
        map((bike: BikeDto) => {
          return {
            message: `Le vélo avec l'id: ${bike.id}, a bien était ajouté !`,
          };
        })
      )
      .toPromise();
  }

  // Supprime un vélo en utilisant la méthode delete du bikeRepository (nécessite: un id qui existe dans la base de données)
  deleteBike(id: number): Promise<HttpException | { message: string }> {
    return from(this.bikeRepository.delete(id))
      .pipe(
        catchError(() => {
          throw new HttpException(
            'Un problème a été rencontré pendant la supression du vélo ',
            HttpStatus.BAD_REQUEST
          );
        }),
        map((bike: UpdateResult) => {
          if (bike.affected === 0) {
            throw new HttpException(
              "Ce vélo n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          return {
            message: `Le vélo avec l'id: ${id}, a bien était supprimé !`,
          };
        })
      )
      .toPromise();
  }

  // Met à jours un vélo en utilisant la méthode save du bikeRepository (nécessite: un id qui existe dans la base de donnée et les informations à modifier)
  async updateBike(
    id: number,
    bikeDto: BikeDto
  ): Promise<HttpException | { message: string }> {
    const bikeEntity = this.getBikeById(id);
    const bikeToUpdate = new BikeEntity();

    // Extraction des données de l'objet BikeDto
    const { longitude, latitude, station, user } = bikeDto;

    // Récupère les informations du vélo qui a cet identifiant
    bikeToUpdate.id = (await bikeEntity).id;
    bikeToUpdate.longitude = (await bikeEntity).longitude;
    bikeToUpdate.latitude = (await bikeEntity).latitude;
    bikeToUpdate.station = (await bikeEntity).station;
    bikeToUpdate.user = (await bikeEntity).user;

    if (longitude) {
      bikeToUpdate.longitude = longitude;
    }
    if (latitude) {
      bikeToUpdate.latitude = latitude;
    }

    // Vérifie d'abord que le vélo pas dans la base de données
    const veloInDb = await this.bikeRepository.findOne({
      where: { user: user },
    });
    if (veloInDb) {
      throw new HttpException(
        'Un utilisateur utilise déjà ce vélo.',
        HttpStatus.NOT_FOUND
      );
    }

    bikeToUpdate.station = station;
    bikeToUpdate.user = user;

    return from(this.bikeRepository.save(bikeToUpdate))
      .pipe(
        map((bike: BikeEntity) => {
          if (!bike) {
            throw new HttpException(
              "Ce vélo n'existe pas.",
              HttpStatus.BAD_REQUEST
            );
          }
          return {
            message: `Le vélo avec l'id: ${bike.id}, a bien était modifié !`,
          };
        })
      )
      .toPromise();
  }
}
