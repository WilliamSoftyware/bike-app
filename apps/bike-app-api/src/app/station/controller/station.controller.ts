import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  HttpStatus,
  ParseIntPipe,
  HttpException,
  UseGuards,
  SetMetadata,
} from '@nestjs/common';
import {
  StationDto,
  stationResourcePath,
} from '@bike-app/bike-api-resource-lib';
import { StationService } from '../service/station.service';
import { AuthGuard } from '@nestjs/passport';
import { RoleGuardGuard } from '../../auth/role-guard.guard';

@Controller(stationResourcePath)
export class StationController {
  constructor(private readonly stationService: StationService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin', 'manager', 'user'])
  @HttpCode(HttpStatus.OK)
  getStations(): Promise<StationDto[]> {
    return this.stationService.getStations();
  }

  @Get('/:id')
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin', 'manager', 'user'])
  getStationsById(@Param('id', ParseIntPipe) id: number): Promise<StationDto> {
    return this.stationService.getStationBy(id);
  }

  @Get('/byParc/:id')
  getStationByParcId(
    @Param('id', ParseIntPipe) id: number
  ): Promise<StationDto[]> {
    return this.stationService.getStationsByParc(id);
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin', 'manager'])
  deleteStation(
    @Param('id', ParseIntPipe) id: number
  ): Promise<HttpException | { message: string }> {
    return this.stationService.deleteStation(id);
  }

  @Post('')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin', 'manager'])
  createStation(
    @Body() stationToCreate: StationDto
  ): Promise<HttpException | { message: string }> {
    return this.stationService.createStation(stationToCreate);
  }

  @Patch('/:id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), RoleGuardGuard)
  @SetMetadata('roles', ['admin', 'manager'])
  updateStation(
    @Param('id', ParseIntPipe) id: number,
    @Body() stationToUpdate: StationDto
  ): Promise<HttpException | { message: string }> {
    return this.stationService.updateStation(id, {
      ...stationToUpdate,
    });
  }
}
