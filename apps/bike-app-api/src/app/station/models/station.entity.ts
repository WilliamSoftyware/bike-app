import { Column, Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from '../../base-entity';
import { ParcEntity } from '../../parc/models/parc.entity';

@Entity()
export class StationEntity extends BaseEntity {
  @Column('text', { nullable: false })
  name: string;

  @Column({ type: 'decimal', precision: 8, scale: 6, nullable: false })
  longitude: number;

  @Column({ type: 'decimal', precision: 8, scale: 6, nullable: false })
  latitude: number;

  @Column({ nullable: false })
  adresse: string;

  @Column({ nullable: false })
  max_places: number;

  @ManyToOne(() => ParcEntity, (parc) => parc.id, { onDelete: 'SET NULL' })
  parcId: ParcEntity;
}
