import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { StationEntity } from '../../station/models/station.entity';
import { from } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { StationDto } from '@bike-app/bike-api-resource-lib';
import { ParcService } from '../../parc/service/parc.service';

@Injectable()
export class StationService {
  constructor(
    @InjectRepository(StationEntity)
    private readonly stationRepository: Repository<StationEntity>,
    private readonly parcService: ParcService
  ) {}

  // Retourne une Promise<StationEntity[]>, la liste des stations
  getStations(): Promise<StationEntity[]> {
    return from(
      this.stationRepository.find({ relations: ['parcId'] })
    ).toPromise();
  }

  // Retourne une Promise<StationEntity> en utilisant la méthode findOne du stationRepository (nécessite: un id qui existe dans la base de données)
  async getStationBy(id: number): Promise<StationEntity> {
    if (!id) {
      throw new HttpException(
        'Veuillez saisir un idenfiant valide.',
        HttpStatus.BAD_REQUEST
      );
    }

    return await from(this.stationRepository.findOne({ id }))
      .pipe(
        catchError(() => {
          throw new HttpException(
            'Un problème a été rencontré pendant la lecture des informations de la station.',
            HttpStatus.BAD_REQUEST
          );
        }),
        map((station: StationEntity) => {
          if (!station) {
            throw new HttpException(
              "Cette station n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          const { ...result } = station;
          return result;
        })
      )
      .toPromise();
  }

  // Retourne une Promise<StationEntity[]>, la liste des vélos présents dans une station
  async getStationsByParc(id: number): Promise<StationEntity[]> {
    return from(
      this.stationRepository
        .createQueryBuilder('station')
        .where('station.parcId = :id', { id })
        .getMany()
    ).toPromise();
  }

  // Créer une station dans la base de donnée en utilisant la méthode save du stationRepository (nécessite: le nom, la longitude, la latitude, l'adresse, le max_places et le parcId)
  async createStation(
    stationDto: StationDto
  ): Promise<HttpException | { message: string }> {
    // Vérification des données dans l'objet StationDto (il manques des informations)
    if (
      !stationDto.name ||
      !stationDto.longitude ||
      !stationDto.latitude ||
      !stationDto.adresse ||
      !stationDto.max_places ||
      !stationDto.parcId.id
    ) {
      throw new HttpException(
        'Veuillez saisir toutes les informations pour créer la station.',
        HttpStatus.BAD_REQUEST
      );
    }

    // Extraction des données de l'objet StationDto
    const { name, longitude, latitude, adresse, max_places, parcId } =
      stationDto;

    // Vérifie d'abord que la station n'existe pas dans la base de données
    const stationInDb = await this.stationRepository.findOne({
      where: { name },
    });
    if (stationInDb) {
      throw new HttpException(
        'La station existe déjà.',
        HttpStatus.BAD_REQUEST
      );
    }

    // Création de l'objet newStation en utilisant les données de StationDto
    const newStation = new StationEntity();
    newStation.name = name;
    newStation.longitude = longitude;
    newStation.latitude = latitude;
    newStation.adresse = adresse;
    newStation.max_places = max_places;
    newStation.parcId = await this.parcService.getParcById(parcId.id);
    newStation.createdAt = new Date(Date.now());
    newStation.updatedAt = new Date(Date.now());

    // Enregistre les informations dans la base de données en utilisant la méthode save du stationRepository
    return from(this.stationRepository.save(newStation))
      .pipe(
        catchError(() => {
          throw new HttpException(
            'Un problème a été rencontré pendant la création de la station ',
            HttpStatus.BAD_REQUEST
          );
        }),
        map((station: StationDto) => {
          return {
            message: `La station avec l'id: ${station.id}, a bien était créée !`,
          };
        })
      )
      .toPromise();
  }

  // Supprime une station en utilisant la méthode delete du stationRepository (nécessite: un id qui existe dans la base de données)
  deleteStation(id: number): Promise<HttpException | { message: string }> {
    return from(this.stationRepository.delete(id))
      .pipe(
        map((station: UpdateResult) => {
          if (station.affected === 0) {
            throw new HttpException(
              "Cette station n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          return {
            message: `La station avec l'id: ${id}, a bien était supprimée !`,
          };
        })
      )
      .toPromise();
  }

  // Met à jours une station en utilisant la méthode save du stationRepository (nécessite: un id qui existe dans la base de donnée et les informations à modifier)
  async updateStation(
    id: number,
    stationDto: StationDto
  ): Promise<HttpException | { message: string }> {
    const stationEntity = this.getStationBy(id);
    const stationToUpdate = new StationEntity();

    // Récupère les informations de la station qui a cet identifiant
    stationToUpdate.id = (await stationEntity).id;
    stationToUpdate.name = (await stationEntity).name;
    stationToUpdate.latitude = (await stationEntity).latitude;
    stationToUpdate.longitude = (await stationEntity).longitude;
    stationToUpdate.adresse = (await stationEntity).adresse;

    // Extraction des données de l'objet stationDto
    const { name, latitude, longitude, adresse } = stationDto;
    if (name) {
      stationToUpdate.name = name;
    }
    if (latitude) {
      stationToUpdate.latitude = latitude;
    }
    if (longitude) {
      stationToUpdate.longitude = longitude;
    }
    if (adresse) {
      stationToUpdate.adresse = adresse;
    }
    stationToUpdate.updatedAt = new Date(Date.now());

    return from(this.stationRepository.save(stationToUpdate))
      .pipe(
        map((station: StationEntity) => {
          if (!station) {
            throw new HttpException(
              "Cette station n'existe pas.",
              HttpStatus.NOT_FOUND
            );
          }
          return {
            message: `La station avec l'id: ${station.id}, a bien était modifiée !`,
          };
        })
      )
      .toPromise();
  }
}
