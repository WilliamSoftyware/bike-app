import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ParcModule } from '../parc/parc.module';
import { StationController } from './controller/station.controller';
import { StationEntity } from './models/station.entity';
import { StationService } from './service/station.service';

@Module({
  imports: [TypeOrmModule.forFeature([StationEntity]), ParcModule],
  controllers: [StationController],
  providers: [StationService],
  exports: [StationService],
})
export class StationModule {}
