import { BikePosition } from '@bike-app/bike-api-resource-lib';
import { SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';



@WebSocketGateway({ cors: ['http://localhost:4200'] })

export class RealTimePositionGateway {
  @WebSocketServer() server: Server;

  constructor() {
  }

  @SubscribeMessage('message')
  handleMessage(client: Socket, payload: BikePosition): string {
    this.server.emit('info', payload);
    return;
  }


}
