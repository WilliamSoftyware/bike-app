import { Test, TestingModule } from '@nestjs/testing';
import { RealTimePositionGateway } from './real-time-position.gateway';

describe('RealTimePositionGateway', () => {
  let gateway: RealTimePositionGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RealTimePositionGateway],
    }).compile();

    gateway = module.get<RealTimePositionGateway>(RealTimePositionGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});
