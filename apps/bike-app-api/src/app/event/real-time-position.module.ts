import { Module } from '@nestjs/common';
import { RealTimePositionGateway } from './real-time-position.gateway';

@Module({
    providers: [RealTimePositionGateway]
})

export class RealTimePositionModule { }
