import { Component, AfterViewInit } from '@angular/core';
import { MapService } from '../services/map.service';
import { StationsService } from '../services/stations.service';

@Component({
  selector: 'bike-app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit {
  constructor(
    private stationsService: StationsService,
    private mapService: MapService,
  ) { }

  map = this.mapService.getMap();

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.mapService.makeMarkerStation();
      this.mapService.setMap(this.map)
    }, 0)
  }

}
