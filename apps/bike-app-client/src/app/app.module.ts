import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminComponent } from './admin/admin.component';
import { ManagerComponent } from './manager/manager.component';
import { UserComponent } from './user/user.component';
import { RouterModule, Routes } from '@angular/router';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { AdminParcsComponent } from './admin/admin-parcs/admin-parcs.component';
import { AdminUsersComponent } from './admin/admin-users/admin-users.component';
import { ManagerVelosComponent } from './manager/manager-velos/manager-velos.component';
import { ManagerStationsComponent } from './manager/manager-stations/manager-stations.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { CreateParcModalComponent } from './admin/admin-parcs/create-parc-modal/create-parc-modal.component';
import { UsersService } from './services/users.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CreateUserModalComponent } from './admin/admin-users/create-user-modal/create-user-modal.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FirstLetterCapsPipe } from './pipes/first-letter-caps.pipe';
import { ParcsService } from './services/parcs.service';
import { AuthGuard } from './services/auth-guard.guard';
import { AuthBearerInterceptor } from '../app/interceptor/auth-bearer.interceptor';
import { HomeComponent } from './home/home.component';
import { SocketIoConfig, SocketIoModule } from 'ngx-socket-io';
import { BikesService } from './services/bikes.service';
import { CreateBikeComponent } from './manager/manager-velos/create-bike/create-bike.component';
import { MapComponent } from './maping/map.component';
import { StationsService } from './services/stations.service';
import { MapService } from './services/map.service';

const routes: Routes = [
  {
    path: 'admin',
    loadChildren: () => import('./admin/admin.module').then(module => module.AdminModule),
    canActivate: [AuthGuard],
    data: {
      role: 'admin'
    }
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module').then(module => module.UserModule),
    canActivate: [AuthGuard],
    data: {
      role: 'user'
    }
  },
  {
    path: 'manager',
    loadChildren: () => import('./manager/manager.module').then(module => module.ManagerModule),
    canActivate: [AuthGuard],
    data: {
      role: 'manager'
    }
  },
  {
    path: 'login',
    loadChildren: () => import('./connexion/connexion.module').then(module => module.ConnexionModule)
  },
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(module => module.HomeModule)
  },
]

const config: SocketIoConfig = { url: '/', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    ManagerComponent,
    UserComponent,
    AdminUsersComponent,
    AdminParcsComponent,
    ManagerVelosComponent,
    ManagerStationsComponent,
    ConnexionComponent,
    InscriptionComponent,
    CreateParcModalComponent,
    CreateUserModalComponent,
    CreateBikeComponent,
    FirstLetterCapsPipe,
    HomeComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    MatTabsModule,
    MatCardModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    HttpClientModule,
    MatDialogModule,
    MatSnackBarModule,
    SocketIoModule.forRoot(config),
  ],
  exports: [
    MatTabsModule,
    MatCardModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
  ],
  entryComponents: [
    CreateUserModalComponent,
    CreateParcModalComponent,
    CreateBikeComponent,
  ],
  providers: [
    UsersService,
    StationsService,
    ParcsService,
    BikesService,
    MapService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthBearerInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
