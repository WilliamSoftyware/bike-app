import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { globalResourcePrefix, authResourcePath } from '@bike-app/bike-api-resource-lib';

@Injectable()
export class AuthBearerInterceptor implements HttpInterceptor {

  private readonly resourceUrl = `${globalResourcePrefix}${authResourcePath}`;

  intercept(request: HttpRequest<unknown>, next: HttpHandler):
    Observable<HttpEvent<unknown>> {
    if (request.url !== 'http://localhost:8000/authentication_token') {
      const token = localStorage.getItem('token');
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }
    return next.handle(request);
  }

}
