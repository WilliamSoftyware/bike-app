import { Component } from '@angular/core';

@Component({
  selector: 'bike-app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.scss'],
})
export class ManagerComponent {
  selectedTab = 0;

  selectTab(event: number) {
    this.selectedTab = event;
  }
}
