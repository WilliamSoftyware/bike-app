import { Component } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { BikeDto } from '@bike-app/bike-api-resource-lib';
import { BikesService } from '../../services/bikes.service';
import { CreateBikeComponent } from './create-bike/create-bike.component';

export interface BikeItem extends BikeDto {
  inEdit: boolean;
}

@Component({
  selector: 'bike-app-manager-velos',
  templateUrl: './manager-velos.component.html',
  styleUrls: ['./manager-velos.component.scss'],
})
export class ManagerVelosComponent {
  displayedColumns: string[] = ['id', 'longitude', 'latitude', 'actions'];

  dataSource = new MatTableDataSource<BikeItem>([]);

  constructor(private dialog: MatDialog, private bikesService: BikesService) {
    this.getBikes();
  }

  getBikes() {
    this.bikesService.findAllBikes().subscribe((data) => {
      this.dataSource.data = data.map((bike) => ({ ...bike, inEdit: false }));
    });
  }

  deleteParc(element: { id: number }) {
    if (!element.id) {
      return;
    }
    this.bikesService.deleteBike(element.id).subscribe(() => {
      this.getBikes();
    });
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();

    const dialogRef = this.dialog.open(CreateBikeComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.bikesService.createBike(result).subscribe(() => {
          this.getBikes();
        });
      }
    });
  }
}
