import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerVelosComponent } from './manager-velos.component';

describe('ManagerVelosComponent', () => {
  let component: ManagerVelosComponent;
  let fixture: ComponentFixture<ManagerVelosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerVelosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerVelosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
