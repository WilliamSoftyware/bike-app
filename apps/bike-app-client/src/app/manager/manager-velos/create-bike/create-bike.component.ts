import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'bike-app-create-bike',
  templateUrl: './create-bike.component.html',
  styleUrls: ['./create-bike.component.scss'],
})
export class CreateBikeComponent {
  constructor(public dialogRef: MatDialogRef<CreateBikeComponent>) {}

  dataForm = new FormGroup({
    longitude: new FormControl(null, Validators.compose([Validators.required])),
    latitude: new FormControl(null, Validators.compose([Validators.required])),
  });

  confirm() {
    this.closeDialog();
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
