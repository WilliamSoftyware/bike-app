import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import {
  BikePosition,
  BikePositionFollow,
  StationDto,
} from '@bike-app/bike-api-resource-lib';
import { Socket } from 'ngx-socket-io';
import { scan } from 'rxjs/operators';
import { MapService } from '../../services/map.service';
import { ParcsService } from '../../services/parcs.service';
import { StationsService } from '../../services/stations.service';

enum STATE {
  WAIT,
  EDIT,
  CREATE,
}

@Component({
  selector: 'bike-app-manager-stations',
  templateUrl: './manager-stations.component.html',
  styleUrls: ['./manager-stations.component.scss'],
})
export class ManagerStationsComponent {
  dataForm = new FormGroup({
    name: new FormControl(null, Validators.compose([Validators.required])),
    adresse: new FormControl(null, Validators.compose([Validators.required])),
    longitude: new FormControl(null, Validators.compose([Validators.required])),
    latitude: new FormControl(null, Validators.compose([Validators.required])),
    max_places: new FormControl(
      null,
      Validators.compose([Validators.required])
    ),
    parcId: new FormControl(null, Validators.compose([Validators.required])),
  });

  readonly parcs$ = this.parcService.findAllParcs();

  dataSource = new MatTableDataSource<BikePositionFollow>([]);
  displayedColumns: string[] = ['id', 'gps', 'follow'];

  followingElement: BikePositionFollow = {
    bikeId: -1,
    longitude: 0,
    latitude: 0,
    follow: false,
  };
  interval: any;

  currentStation: StationDto = {
    name: '',
    id: 0,
    longitude: 0,
    latitude: 0,
    adresse: '',
    max_places: 0,
    parcId: {
      name: '',
      id: 0,
      longitude: 0,
      latitude: 0,
      city: '',
    },
  };

  STATE = STATE;
  currentState: STATE = STATE.WAIT;

  constructor(
    private socket: Socket,
    private parcService: ParcsService,
    private stationService: StationsService,
    private mapService: MapService
  ) {
    this.mapService.currentStationChanged$.subscribe((station: StationDto) => {
      if (!station.id) {
        return;
      }

      this.currentStation = station;

      const { name, adresse, longitude, latitude, max_places, parcId } =
        station;
      this.dataForm.setValue({
        name,
        adresse,
        longitude,
        latitude,
        max_places,
        parcId: parcId.id,
      });

      this.currentState = STATE.EDIT;
    });
  }

  //get infos from users using bike who shared their position
  readonly data$ = this.socket
    .fromEvent<BikePosition>('info')
    .pipe(
      scan((acc: BikePositionFollow[], item: BikePosition) => {
        const index = acc.findIndex((bike) => bike.bikeId === item.bikeId);
        if (index === -1) {
          acc.push({ ...item, follow: false });
        } else {
          if (acc[index].follow) {
            this.followingElement = { ...acc[index], ...item };
          }
          acc[index] = { ...acc[index], ...item };
        }
        return acc;
      }, [])
    )
    .subscribe((test) => (this.dataSource.data = [...test]));

  followOnMap(element: BikePositionFollow) {
    element.follow = true;
    const { longitude, latitude } = this.followingElement;
    this.mapService.makeMarkerBike(longitude, latitude).then();

    this.interval = setInterval(() => {
      const { longitude, latitude } = this.followingElement;
      this.mapService.moveMarkerBike(longitude, latitude).then();
    }, 2000);
  }

  stopFollow(element: BikePositionFollow) {
    element.follow = false;
  }

  createStation() {
    this.stationService.createStation(this.dataForm.value).subscribe();
    this.mapService.resetMap();
    this.currentState = STATE.WAIT;
  }

  startCreateStation() {
    this.dataForm.reset();
    this.currentState = STATE.CREATE;
  }

  stopCreateStation() {
    this.currentState = STATE.WAIT;
  }

  startEditStation() {
    this.stationService
      .editStation({ ...this.dataForm.value, id: this.currentStation.id })
      .subscribe();
    this.currentState = STATE.EDIT;
  }

  stopEditStation() {
    this.currentState = STATE.WAIT;
  }

  deleteStation() {
    this.stationService.deleteStation(this.currentStation).subscribe();
    this.mapService.resetMap();
    this.currentState = STATE.WAIT;
  }
}
