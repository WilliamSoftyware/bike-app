import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerStationsComponent } from './manager-stations.component';

describe('ManagerStationsComponent', () => {
  let component: ManagerStationsComponent;
  let fixture: ComponentFixture<ManagerStationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerStationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerStationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
