import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { UsersService } from '../services/users.service';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent
  },
]


@NgModule({
  declarations: [
  ],
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
  ],
  providers: [
    UsersService,
  ]

})
export class AdminModule { }
