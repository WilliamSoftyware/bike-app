import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminParcsComponent } from './admin-parcs.component';

describe('AdminParcsComponent', () => {
  let component: AdminParcsComponent;
  let fixture: ComponentFixture<AdminParcsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminParcsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminParcsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
