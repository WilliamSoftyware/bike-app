import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateParcModalComponent } from './create-parc-modal.component';

describe('CreateParcModalComponent', () => {
  let component: CreateParcModalComponent;
  let fixture: ComponentFixture<CreateParcModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateParcModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateParcModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
