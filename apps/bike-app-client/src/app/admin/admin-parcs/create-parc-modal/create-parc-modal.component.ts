import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'bike-app-create-parc-modal',
  templateUrl: './create-parc-modal.component.html',
  styleUrls: ['./create-parc-modal.component.scss']
})
export class CreateParcModalComponent {

  constructor(public dialogRef: MatDialogRef<CreateParcModalComponent>) {
  }

  dataForm = new FormGroup({
    name: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    city: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    longitude: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    latitude: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
  })

  confirm() {
    this.closeDialog();
  }

  closeDialog() {
    this.dialogRef.close();
  }

}
