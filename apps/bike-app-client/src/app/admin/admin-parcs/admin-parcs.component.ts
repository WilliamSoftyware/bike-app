import { Component, } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { ParcsService } from '../../services/parcs.service';
import { CreateParcModalComponent } from './create-parc-modal/create-parc-modal.component';
import { ParcDto } from '@bike-app/bike-api-resource-lib';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface ParcItem extends ParcDto {
  inEdit?: boolean;
}

@Component({
  selector: 'bike-app-admin-parcs',
  templateUrl: './admin-parcs.component.html',
  styleUrls: ['./admin-parcs.component.scss']
})
export class AdminParcsComponent {
  displayedColumns: string[] = ['name', 'longitude', 'latitude', 'city', 'actions'];

  dataSource = new MatTableDataSource<ParcItem>([]);

  defaultElement: ParcItem = {
    id: 0,
    name: '',
    longitude: 0,
    latitude: 0,
    city: '',
    inEdit: false,
  };

  dataForm = new FormGroup({
    name: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    longitude: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    latitude: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    city: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
  });

  constructor(
    private dialog: MatDialog,
    private parcService: ParcsService,
    private _snackBar: MatSnackBar
  ) {
    this.getParcs();
  }

  getParcs() {
    this.parcService.findAllParcs().subscribe((data) => {
      this.dataSource.data = data.map((parc) => ({ ...parc, inEdit: false }));
    })
  }

  startEdit(element: ParcItem) {
    if (this.rowIsInEdit()) {
      return;
    }
    const { name, longitude, city, latitude } = element
    this.dataForm.patchValue({
      name,
      longitude,
      latitude,
      city,
    })
    this.defaultElement = { ...element };

    element.inEdit = true;
  }

  cancelEdit(element: {inEdit: boolean}) {
    element.inEdit = false;
  }

  validEdit(element: ParcItem) {
    Object.assign(element, this.dataForm.value);
    delete element.inEdit
    this.parcService.updateParc(element).subscribe();
    element.inEdit = false;
  }

  rowIsInEdit() {
    const rowIsInEdit = this.dataSource.data.some((user) => user.inEdit);
    if (rowIsInEdit) {
      this._snackBar.open('A parc is already in edit.', 'Close', { duration: 2000 });
    }
    return rowIsInEdit;
  }

  deleteParc(element: Record<string, number>) {
    if (!element.id) {
      return;
    }
    this.parcService.deleteParc(element.id).subscribe(
      () => {
        this.getParcs();
      }
    );
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();

    const dialogRef = this.dialog.open(CreateParcModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.parcService.createParc(result).subscribe(
          () => {
            this.getParcs();
          }
        )
      }
    });
  }

}
