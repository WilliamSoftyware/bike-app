import { Component } from '@angular/core';

export interface TabItem {
  label: string;
  route: string;
}

@Component({
  selector: 'bike-app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {
  selectedTab = 0;

  selectTab(event: number) {
    this.selectedTab = event;
  }

}
