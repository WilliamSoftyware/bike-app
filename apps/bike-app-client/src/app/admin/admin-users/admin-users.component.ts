import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { UsersService } from '../../services/users.service';
import { UserDto, UserRole } from '@bike-app/bike-api-resource-lib';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { CreateUserModalComponent } from './create-user-modal/create-user-modal.component';
import { MatSnackBar } from '@angular/material/snack-bar';

export interface UserItem extends UserDto {
  inEdit?: boolean;
}

@Component({
  selector: 'bike-app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent {
  displayedColumns: string[] = ['firstName', 'lastName', 'email', 'role', 'actions'];
  ROLES_DATA = ['user','admin','manager']

  dataSource = new MatTableDataSource<UserItem>([]);

  defaultElement: UserItem = {
    id: 0,
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    role: UserRole.USER,
    createdAt: new Date(),
    updatedAt: new Date(),
    inEdit: false,
  };

  dataForm = new FormGroup({
    firstName: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    lastName: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    email: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    role: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
  });

  constructor(
    private userService: UsersService,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {
    this.getUsers();
  }

  startEdit(element: UserItem) {
    if (this.rowIsInEdit()) {
      return;
    }
    const { firstName, lastName, email, role } = element
    this.dataForm.patchValue({
      firstName,
      lastName,
      email,
      role,
    })
    this.defaultElement = { ...element };

    element.inEdit = true;
  }

  getUsers() {
    this.userService.findAllUsers().subscribe((data) => {
      this.dataSource.data = data.map((user) => ({ ...user, inEdit: false }));
    })
  }

  cancelEdit(element: UserItem) {
    element.inEdit = false;
  }

  validEdit(element: UserItem) {
    Object.assign(element, this.dataForm.value);
    delete element.inEdit
    this.userService.updateUser(element).subscribe();
    element.inEdit = false;
  }

  rowIsInEdit() {
    const rowIsInEdit = this.dataSource.data.some((user) => user.inEdit);
    if (rowIsInEdit) {
      this._snackBar.open('An user is already in edit.', 'Close', { duration: 2000 });
    }
    return rowIsInEdit;
  }

  deleteUser(element: UserItem) {
    if (!element.id) {
      return;
    }
    this.userService.deleteUser(element.id).subscribe(
      () => {
        this.getUsers();
      }
    );
  }

  openModal() {
    const dialogConfig = new MatDialogConfig();

    const dialogRef = this.dialog.open(CreateUserModalComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.userService.createUser(result).subscribe(
          () => {
            this.getUsers();
          }
        )
      }
    });

  }
}
