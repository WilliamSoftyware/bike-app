import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { UserRole } from '@bike-app/bike-api-resource-lib';

@Component({
  selector: 'bike-app-create-user-modal',
  templateUrl: './create-user-modal.component.html',
  styleUrls: ['./create-user-modal.component.scss']
})

export class CreateUserModalComponent {
  roleValues = Object.values(UserRole);

  constructor(public dialogRef: MatDialogRef<CreateUserModalComponent>) {}

  dataForm = new FormGroup({
    firstName: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    lastName: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    email: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    password: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
    role: new FormControl(null, Validators.compose([
      Validators.required,
    ])),
  })

  confirm() {
    this.closeDialog();
  }

  closeDialog() {
    this.dialogRef.close();
  }

}
