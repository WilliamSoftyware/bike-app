import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'bike-app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss'],
})
export class ConnexionComponent {
  dataForm = new FormGroup({
    email: new FormControl(null, Validators.compose([Validators.required])),
    password: new FormControl(null, Validators.compose([Validators.required])),
  });

  constructor(private authService: AuthenticationService) {}

  connection() {
    const { email, password } = this.dataForm.value;
    this.authService.authentication(email, password);
  }
}
