import { Component } from '@angular/core';
import { StationDto } from '@bike-app/bike-api-resource-lib';
import { Socket } from 'ngx-socket-io';
import { AuthenticationService } from '../services/authentication.service';
import { MapService } from '../services/map.service';

@Component({
  selector: 'bike-app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent {
  currentStation: StationDto = {
    name: '',
    id: 0,
    longitude: 0,
    latitude: 0,
    adresse: '',
    max_places: 0,
    parcId: {
      name: '',
      id: 0,
      longitude: 0,
      latitude: 0,
      city: '',
    },
  };

  socketInterval: any;

  userId = this.authService.getUserId();

  constructor(
    private socket: Socket,
    private authService: AuthenticationService,
    private mapService: MapService
  ) {
    this.mapService.currentStationChanged$.subscribe((station: StationDto) => {
      this.currentStation = station;
    });
  }

  track() {
    if (!navigator.geolocation) {
      return;
    }

    this.socketInterval = setInterval(() => {
      navigator.geolocation.getCurrentPosition((position) => {
        this.socket.emit('message', {
          bikeId: this.userId,
          longitude: position.coords.longitude,
          latitude: position.coords.latitude,
        });
      });
    }, 1000);
  }
}
