import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {
  globalResourcePrefix,
  parcResourcePath,
  ParcDto,
} from '@bike-app/bike-api-resource-lib';

@Injectable({
  providedIn: 'root',
})
export class ParcsService {
  private readonly resourceUrl = `${globalResourcePrefix}${parcResourcePath}`;

  constructor(private httpClient: HttpClient, private _snackBar: MatSnackBar) {}

  findAllParcs(): Observable<ParcDto[]> {
    return this.httpClient.get<ParcDto[]>(this.resourceUrl).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  updateParc(parc: ParcDto) {
    return this.httpClient.patch(`${this.resourceUrl}/${parc}`, parc).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  createParc(parc: ParcDto) {
    return this.httpClient.post(this.resourceUrl, parc).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  deleteParc(id: number) {
    return this.httpClient.delete(`${this.resourceUrl}/${id}`).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  printErrorMessage(errorStatus: number) {
    this._snackBar.open(`Une erreur ${errorStatus} s'est produite.`, 'Fermer', {
      duration: 2000,
    });
  }
}
