import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard {

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private _snackBar: MatSnackBar,
  ) { }

  canActivate(route: ActivatedRouteSnapshot) {
    if (!this.authService.isLogedIn()) {
      this.router.navigateByUrl('/');
      this._snackBar.open(`Vous devez être connecté pour accéder à cette application.`, 'Fermer', { duration: 2000 });
      return false;
    }
    if (!this.authService.checkRole(route.data.role)) {
      this.router.navigateByUrl('/');
      this._snackBar.open(`Vous n'avez pas le droit d'accéder à cette partie de l'application.`, 'Fermer', { duration: 2000 });
      return false;
    }
    return true;
  }
}
