import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  BikeDto,
  bikeResourcePath,
  globalResourcePrefix,
} from '@bike-app/bike-api-resource-lib';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class BikesService {
  private readonly resourceUrl = `${globalResourcePrefix}${bikeResourcePath}`;

  constructor(private httpClient: HttpClient, private _snackBar: MatSnackBar) {}

  findAllBikes(): Observable<BikeDto[]> {
    return this.httpClient.get<BikeDto[]>(this.resourceUrl).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  updateBike(bike: BikeDto) {
    return this.httpClient.patch(`${this.resourceUrl}/${bike}`, bike).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  createBike(bike: BikeDto) {
    return this.httpClient.post(this.resourceUrl, bike).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  deleteBike(id: number) {
    return this.httpClient.delete(`${this.resourceUrl}/${id}`).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  printErrorMessage(errorStatus: number) {
    this._snackBar.open(`Une erreur ${errorStatus} s'est produite.`, 'Fermer', {
      duration: 2000,
    });
  }
}
