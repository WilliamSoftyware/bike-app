import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { globalResourcePrefix, StationDto, stationResourcePath } from '@bike-app/bike-api-resource-lib';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StationsService {

  private readonly resourceUrl = `${globalResourcePrefix}${stationResourcePath}`;

  constructor(
    private httpClient: HttpClient,
    private _snackBar: MatSnackBar,
  ) { }

  findAllStations(): Promise<StationDto[]> {
    return this.httpClient.get<StationDto[]>(this.resourceUrl).toPromise()
      ;
  }

  createStation(station: StationDto): Observable<any> {
    return this.httpClient.post(this.resourceUrl, {
      ...station,
      parcId: { id: station.parcId }
    }).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  editStation(station: StationDto): Observable<any> {
    return this.httpClient.patch(`${this.resourceUrl}/${station.id}`, {
      ...station,
      parcId: { id: station.parcId }
    }).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  deleteStation(station: StationDto): Observable<any> {
    return this.httpClient.delete(`${this.resourceUrl}/${station.id}`).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  printErrorMessage(errorStatus: number) {
    this._snackBar.open(`Une erreur ${errorStatus} s'est produite.`, 'Fermer', { duration: 2000 });
  }
}
