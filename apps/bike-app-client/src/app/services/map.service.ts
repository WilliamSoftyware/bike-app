import { Injectable } from '@angular/core';
import { StationDto } from '@bike-app/bike-api-resource-lib';
import * as L from 'leaflet';
import { BehaviorSubject, Observable } from 'rxjs';
import { StationsService } from './stations.service';

@Injectable({
  providedIn: 'root',
})
export class MapService {
  private currentStation$: BehaviorSubject<StationDto> =
    new BehaviorSubject<StationDto>({
      name: '',
      id: 0,
      longitude: 0,
      latitude: 0,
      adresse: '',
      max_places: 0,
      parcId: {
        name: '',
        id: 0,
        longitude: 0,
        latitude: 0,
        city: '',
      },
    });
  currentStationChanged$: Observable<StationDto> =
    this.currentStation$.asObservable();
  private currentStation: StationDto = {
    name: '',
    id: 0,
    longitude: 0,
    latitude: 0,
    adresse: '',
    max_places: 0,
    parcId: {
      name: '',
      id: 0,
      longitude: 0,
      latitude: 0,
      city: '',
    },
  };

  private map: any;
  private marker: any;

  constructor(private stationsService: StationsService) {}

  async makeMarkerBike(longitude: number, latitude: number) {
    // create icon
    const bikeIcon = L.icon({
      iconUrl: 'assets/bike.png',
      iconSize: [38, 50],
    });

    // put stations on the map

    L.Marker.prototype.options.icon = bikeIcon;
    this.marker = L.marker([latitude, longitude], {
      icon: bikeIcon,
    });
    this.map.addLayer(this.marker);
  }

  async makeMarkerStation() {
    // Get stations from the database
    const stations = await this.stationsService.findAllStations();

    // Create icon
    const greenIcon = L.icon({
      iconUrl: 'assets/Map_icon_green.png',
      iconSize: [30, 35],
    });

    // put stations on the map
    stations.forEach((station) => {
      L.Marker.prototype.options.icon = greenIcon;
      L.marker([station.latitude, station.longitude], {
        icon: greenIcon,
      })
        .addTo(this.map)
        .on('click', this.onClick.bind(this, station));
    });
  }

  async moveMarkerBike(longitude: number, latitude: number) {
    const newLatLng = new L.LatLng(latitude, longitude);
    this.marker.setLatLng(newLatLng);
  }

  getMap() {
    return this.map;
  }

  setMap(map: any) {
    this.map = map;
    this.initMap();
  }

  setCurrentStation(station: StationDto) {
    this.currentStation = station;
    this.currentStation$.next(this.currentStation);
  }

  onClick(station: StationDto) {
    this.setCurrentStation(station);
  }

  resetMap() {
    this.map.off();
    this.map.remove();
    this.makeMarkerStation();
    this.initMap();
  }

  initMap(): void {
    this.map = L.map('map', {
      center: [43.604652, 1.444209], // In Leaflet you need to make begin the map somewhere, here is Toulouse city
      zoom: 13,
    });

    // Add this layer to see the map
    const tiles = L.tileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {
        maxZoom: 18,
        minZoom: 3,
        attribution:
          '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      }
    );
    tiles.addTo(this.map);
  }
}
