import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  globalResourcePrefix,
  userResourcePath,
  UserDto,
} from '@bike-app/bike-api-resource-lib';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class UsersService {
  private readonly resourceUrl = `${globalResourcePrefix}${userResourcePath}`;

  constructor(private httpClient: HttpClient, private _snackBar: MatSnackBar) {}

  findAllUsers(): Observable<UserDto[]> {
    return this.httpClient.get<UserDto[]>(this.resourceUrl).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  updateUser(user: UserDto) {
    return this.httpClient.patch(`${this.resourceUrl}/${user.id}`, user).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  createUser(user: UserDto) {
    return this.httpClient.post(this.resourceUrl, user).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  deleteUser(id: number) {
    return this.httpClient.delete(`${this.resourceUrl}/${id}`).pipe(
      catchError((err: HttpErrorResponse) => {
        this.printErrorMessage(err.status);
        return throwError(err);
      })
    );
  }

  printErrorMessage(errorStatus: number) {
    this._snackBar.open(`Une erreur ${errorStatus} s'est produite.`, 'Fermer', {
      duration: 2000,
    });
  }
}
