import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  globalResourcePrefix,
  authResourcePath,
  JwtContent,
} from '@bike-app/bike-api-resource-lib';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  private readonly resourceUrl = `${globalResourcePrefix}${authResourcePath}`;

  constructor(
    private httpClient: HttpClient, 
    private router: Router,
    private _snackBar: MatSnackBar
    ) {}

  authentication(email: string, password: string): void {
    const requestOptions: Record<string, string> = {
      responseType: 'text',
    };

    this.httpClient
      .post<string>(
        `${this.resourceUrl}/login`,
        { email, password },
        requestOptions
      )
      .pipe(
        catchError((err: HttpErrorResponse) => {
          if(err.status === 401){
            this._snackBar.open('Email ou mot de passe incorrect.', 'Fermer', { duration: 2000 });
          }
          return throwError(err);
        })
      )
      .subscribe((token) => {
        localStorage.setItem('token', token);
        const decodedJwt: JwtContent = jwt_decode(token);
        this.router.navigateByUrl(`/${decodedJwt.role}`);
      });
  }

  isLogedIn(): boolean {
    const token = localStorage.getItem('token');
    if (!token) {
      return false;
    }
    return true;
  }

  checkRole(role: string): boolean {
    const token = localStorage.getItem('token');
    if (!token) {
      return false;
    }
    const decodedJwt: JwtContent = jwt_decode(token);
    return decodedJwt.role === role;
  }

  getUserId(): number{
    const token = localStorage.getItem('token');
    if (!token) {
      this._snackBar.open('Vous devez être authentifié.', 'Fermer', { duration: 2000 });
      return 0;
    }
    const decodedJwt: JwtContent = jwt_decode(token);
    return decodedJwt.id;
  }
}
