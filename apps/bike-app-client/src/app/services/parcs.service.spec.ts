import { TestBed } from '@angular/core/testing';

import { ParcsService } from './parcs.service';

describe('ParcsService', () => {
  let service: ParcsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParcsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
