import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "firstLetterCaps"
})

export class FirstLetterCapsPipe implements PipeTransform {
    transform(str: string) {
        if (!str) {
            return str;
        }
        return str[0].toUpperCase() + str.substring(1).toLowerCase();
    }
}