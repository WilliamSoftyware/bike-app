export const globalResourcePrefix = 'api';
export * from './lib/bike.resource';
export * from './lib/parc.resource';
export * from './lib/station.resource';
export * from './lib/user.resource';
export * from './lib/auth.resource';
