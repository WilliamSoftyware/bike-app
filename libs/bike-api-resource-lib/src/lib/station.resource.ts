import { ParcDto } from '..';

export const stationResourcePath = '/station';

export interface StationDto {
  id: number;
  name: string;
  longitude: number;
  latitude: number;
  adresse: string;
  max_places: number;
  parcId: ParcDto;
  createdAt?: Date;
  updatedAt?: Date;
}
