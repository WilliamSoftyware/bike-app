import { StationDto, UserDto } from '..';

export const bikeResourcePath = '/bike';

export interface BikeDto {
  id: number;
  longitude: number;
  latitude: number;
  station?: StationDto;
  user?: UserDto;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface BikePosition {
  bikeId : number;
  longitude: number;
  latitude: number;
}

export interface BikePositionFollow extends BikePosition {
  follow : boolean;
}
