export const parcResourcePath = '/parc';

export interface ParcDto {
  id: number;
  name: string;
  longitude: number;
  latitude: number;
  city: string;
  createdAt?: Date;
  updatedAt?: Date;
}
