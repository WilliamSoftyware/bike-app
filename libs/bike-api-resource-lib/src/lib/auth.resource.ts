import { UserRole } from "..";

export const authResourcePath = '/auth';

export interface JwtContent{
    id:number,
    role: UserRole,
}
